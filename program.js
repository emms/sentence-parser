
/**
 * Shorthen a word into:
 * first letter + number of distinct characters in between first and last letter + last letter
 * @param {} word 
 * @returns New word e.g. Smooth => S3h 
 */
function shortenWord(word) {
    let chars = {};
    const length = word.length;

    // Ignores first and last chars
    for (let i = 1; i <= length -2; i++) {
        chars[word[i]] = 1;
    }

    const totalDistChars = Object.keys(chars).length;
    const result = word[0] + totalDistChars + word[length - 1];

    return result;
}

/**
 * Shorten each word in a complete sentence
 * @param {*} sentence 
 * @returns
 */
function sorthenSentence(sentence) {
    const separator = /[^a-zA-Z\d:]/;
    const words = sentence.split(separator);

    // Replace each word by its shorten
    for (const word of words) {
        sentence = sentence.replace(word, shortenWord(word));
    }

    return sentence;
}

var response = sorthenSentence('Everything is awesome');
console.log(response);
